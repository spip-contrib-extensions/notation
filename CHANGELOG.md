# Changelog du plugin Notations

## 3.2.0 - 2025-02-27

### Changed

- Compatible SPIP 4.2 minimum
- Chaînes de langue au format SPIP 4.1+

## 3.1.0 - 2024-07-05

### Changed

- Compatible SPIP 4.*